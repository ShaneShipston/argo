const { src, dest } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const minifycss = require('gulp-clean-css');
const webpack = require('webpack');
const gulpWebpack = require('webpack-stream');
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

const config = {
    src: {
        js: 'src/js/**/*.js',
        css: 'src/scss/**/*.scss',
    },
    dest: {
        js: 'javascripts/renderer/',
        css: 'stylesheets',
    },
    webpack: {
        mode: 'production',
        entry: {
            application: path.resolve(__dirname, 'src/js/application.js'),
        },
        output: {
            path: path.resolve(__dirname, 'javascripts/renderer'),
            filename: '[name].js',
        },
        target: 'electron-renderer',
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    exclude: /node_modules/,
                    use: 'vue-loader',
                },
            ],
        },
        plugins: [
            new VueLoaderPlugin(),
        ],
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@': path.join(__dirname, 'src', 'js'),
            },
        },
    },
};

function styles(cb) {
    src(config.src.css)
        .pipe(sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        .pipe(minifycss())
        .pipe(dest(config.dest.css));

    cb();
}

function javascript(cb) {
    src(config.src.js)
        .pipe(gulpWebpack(config.webpack, webpack))
        .pipe(dest(config.dest.js));

    cb();
}

exports.styles = styles;
exports.javascript = javascript;
exports.init = javascript;
exports.default = javascript;
