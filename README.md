# Argo

> Desktop application to assist with building project assets

## Get started

```
$ npm install && npm install -g electron
$ npm run build
```

### Run

```
$ npm start
```
