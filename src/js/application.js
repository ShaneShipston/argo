/* global Vue */
import router from './util/router';
import store from './util/store';
import Settings from '@/settings';
import Toolbar from '@/components/Toolbar.vue';
import SystemCheck from '@/components/SystemCheck.vue';
import CommandBus from '@/components/CommandBus.vue';

Vue.component('toolbar', Toolbar);
Vue.component('system-check', SystemCheck);
Vue.component('command-bus', CommandBus);

Vue.config.productionTip = false;

window.eventbus = new Vue();
window.storage = new Settings();

window.argo = new Vue({
    router,
    store,
}).$mount('#wrapper');

/**
 * External Links
 */
document.addEventListener('click', (event) => {
    if (event.target.tagName === 'A' && event.target.href.startsWith('http')) {
        event.preventDefault();
    }
});
