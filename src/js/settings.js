import Store from './store';
import electron from 'electron';
import path from 'path';

const homePath = (electron.app || electron.remote.app).getPath('home');

const appDefaults = {
    directory: path.join(homePath, 'argo'),
};

export default class Settings extends Store {
    constructor() {
        super({
            configName: 'app-settings',
            defaults: appDefaults,
        });
    }
    restore() {
        this.assign(appDefaults);
    }
}
