/* global Vue */
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        project: null,
        logs: '',
        errors: '',
        hasNewErrors: false,
    },

    mutations: {
        project(state, payload) {
            state.project = payload;
        },
        log(state, payload) {
            state.logs += payload;
        },
        error(state, payload) {
            state.errors += payload;
            state.hasNewErrors = true;
        },
        clear(state) {
            state.logs = '';
            state.errors = '';
            state.hasNewErrors = false;
        },
        errorsViewed(state) {
            state.hasNewErrors = false;
        },
    },
});
