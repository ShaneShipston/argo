import path from 'path';

export function slug(input) {
    return input.toString().toLowerCase().trim()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-');        // Replace multiple - with single -
}

export function storagePath(item) {
    return path.join(window.storage.get('directory'), item);
}

export function config(item) {
    return storagePath(`configs/${item}.js`);
}
