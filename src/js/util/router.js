/* global Vue */
import Router from 'vue-router';
import Dashboard from './../views/Dashboard.vue';
import NewProject from './../views/NewProject.vue';
import EditProject from './../views/EditProject.vue';
import Settings from './../views/Settings.vue';
import Terminal from './../views/Terminal.vue';
import Import from './../views/Import.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Dashboard,
        },
        {
            path: '/new',
            component: NewProject,
        },
        {
            path: '/settings',
            component: Settings,
        },
        {
            path: '/terminal',
            component: Terminal,
        },
        {
            path: '/edit/:path',
            component: EditProject,
            props: true,
        },
        {
            path: '/import',
            component: Import,
        },
    ],
});
