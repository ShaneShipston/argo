const { app, BrowserWindow, ipcMain } = require('electron');
const windowStateKeeper = require('electron-window-state');
const path = require('path');
const json = require('../../package.json');
const kill = require('tree-kill');

const pids = [];
let argoWindow;

ipcMain.on('new-pid', (event, arg) => {
    pids.push(arg);
});

ipcMain.on('remove-pid', (event, arg) => {
    const index = pids.indexOf(arg);

    if (index >= 0) {
        pids.splice(index, 1);
    }
});

app.on('ready', () => {
    const mainWindowState = windowStateKeeper({
        defaultWidth: json.settings.width,
        defaultHeight: json.settings.height,
    });

    // BrowserWindow.removeDevToolsExtension('Vue.js devtools');
    // BrowserWindow.addDevToolsExtension('C:\\Users\\Shane\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\nhdogjmejiglipccpnnnanhbledajbpd\\5.3.3_0');

    argoWindow = new BrowserWindow({
        title: json.name,
        x: mainWindowState.x,
        y: mainWindowState.y,
        width: mainWindowState.width,
        height: mainWindowState.height,
        minWidth: 500,
        minHeight: 400,
        frame: false,
        backgroundColor: '#2a3542',
        webPreferences: {
            nodeIntegration: true,
        },
    });

    mainWindowState.manage(argoWindow);

    argoWindow.loadURL(`file://${path.resolve(__dirname, '../../index.html')}`);

    argoWindow.webContents.on('did-finish-load', () => {
        argoWindow.webContents.send('loaded', {
            appName: json.name,
            appVersion: json.version,
        });

        // argoWindow.webContents.openDevTools();
    });

    argoWindow.on('closed', () => {
        argoWindow = null;
    });
});

app.on('before-quit', (event) => {
    if (pids.length > 0) {
        event.preventDefault();
    }

    pids.forEach((pid) => {
        kill(pid, () => {
            const index = pids.indexOf(pid);

            if (index >= 0) {
                pids.splice(index, 1);
            }

            if (pids.length === 0) {
                app.quit();
            }
        });
    });
});
