const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    mode: 'production',
    entry: path.resolve(__dirname, 'src/js/application.js'),
    output: {
        path: path.resolve(__dirname, 'javascripts/renderer'),
        filename: 'application.js',
    },
    target: 'electron-renderer',
    module: {
        rules: [
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: 'vue-loader',
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.join(__dirname, 'src', 'js'),
        },
    },
};
